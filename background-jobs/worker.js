const { Worker } = require("bullmq");

const db = require("../models");

const { processOrganizationQueueName } = require("./constants");

(async () => {
  await db.waitForDb();

  const worker = new Worker(
    processOrganizationQueueName,
    async (job) => {
      const org = job.data.organization;
      const enqueuedBy = job.data.enqueuedBy;

      console.log(`Organization ${org.id} is being processed.`);
      await new Promise((resolve) => setTimeout(() => resolve(), 1000));
      await db.OrganizationStatus.create({
        status: "PROCESSED",
        UserId: enqueuedBy,
        OrganizationId: org.id,
      });
    },
    {
      connection: { host: process.env.REDIS_HOSTNAME },
    }
  );

  worker.on("completed", (job) => {
    console.log(`Job #${job.id} has completed!`);
  });
})();
