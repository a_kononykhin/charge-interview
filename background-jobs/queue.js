const { Queue } = require("bullmq");
const { processOrganizationQueueName } = require("./constants");

const processOrganizationQueue = new Queue(processOrganizationQueueName, {
  connection: { host: process.env.REDIS_HOSTNAME },
});

exports.addProcessOrganizationJob = async function (organization, enqueuedBy) {
  await processOrganizationQueue.add("process", { organization, enqueuedBy });
};
