"use strict";

const uuid = require("uuid/v4");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const userId = uuid();

    await queryInterface.bulkInsert("Users", [
      {
        id: userId,
        identifier: "admin@example.com",
        createdAt: new Date(),
        updatedAt: new Date(),
        api_token: process.env.API_KEY,
      },
    ]);

    await queryInterface.bulkInsert("DataSources", [
      {
        id: userId,
        method: "api",
        version: "1.0",
        createdAt: new Date(),
        UserId: userId,
      },
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
