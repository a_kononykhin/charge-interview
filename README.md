# Charge Full Stack API Take Home Result

To start this app you need to add .env file first. Example:

```dosini
NODE_ENV=development
DB_NAME=charge-db
DB_USERNAME=server
DB_PASSWORD=password
API_KEY=41a2e03f8686640344badf516a008503
```

Now you can start app using docker compose. With current configuration it will start both server and worker in watch mode.
Before making any requests, make sure to synchronize sequelize models with database and seed admin user.

## Answers

Q: How would you scale this API to process millions of requests?

A: I would start with some system for management of containerized applications like Kubernetes or Docker Swarm to scale horizontally. The next step probably would be to optimize DB requests.


Q: What you think would make this API more secure or easily extensible?

A: To name a few:

- close all ports used for debugging/development for external acces

- enable HTTPS and froce redirect from HTTP

- replace hardcoded API key with real authentication

- setup regular DB backups

- implement rate-limiting to prevent brute force attacks

- make sure sensetive information doesn't leak when error occurs
