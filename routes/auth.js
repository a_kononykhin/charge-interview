const db = require("../models");

exports.authMiddleware = async function (req, res, next) {
  const token = req.headers["x-api-key"];

  if (!token) {
    res.status(401).json({ message: "API key is not provided" });
    return;
  }

  const user = await db.User.findOne({
    where: { api_token: token },
    attributes: ["id", "identifier"],
  });
  if (user === null) {
    res.status(401).json({ message: "Invalid API key" });
    return;
  } else {
    req.currentUser = user;
    next();
  }
};
