const express = require("express");
const router = express.Router();

const db = require("../models");
const { addProcessOrganizationJob } = require("../background-jobs/queue");
const { formatSequelizeError } = require("../utils");

const mapToResponseFormat = (org, dataFormat) => {
  const result = {
    id: org.id,
    identifier: org.identifier,
    source: {
      method: org.DataSource.method,
      version: org.DataSource.version,
      user: {
        id: org.DataSource.User.id,
        identifier: org.DataSource.User.identifier,
      },
      createdAt: org.DataSource.createdAt,
    },
    data: {},

    // org.OrganizationStatuses is sorted by created date already
    result: {
      status: org.OrganizationStatuses[0].status,
      codes: [],
      statusHistory: org.OrganizationStatuses.map(
        ({ status, User, createdAt }) => ({
          status,
          user: {
            id: User.id,
            identifier: User.identifier,
          },
          createdAt,
        })
      ),
      checks: [],
    },
    notes: [],
    updatedAt: org.updatedAt,
    createdAt: org.createdAt,
  };

  if (dataFormat === "full" || dataFormat === "base64") {
    const organization = { ...org };
    organization.additionalChecks = organization.AdditionalChecks.map(
      ({ name, data }) => ({
        name,
        data,
      })
    );
    delete organization.AdditionalChecks;
    delete organization.OrganizationStatuses;
    delete organization.DataSource;
    delete organization.DataSourceId;
    result.data = { organization };
    if (dataFormat === "base64") {
      result.data = Buffer.from(JSON.stringify(result.data)).toString("base64");
    }
  }

  return result;
};

const listOrganizations = async function (req, res) {
  const { data } = req.query;
  const needFullInfo = data && (data === "full" || data === "base64");

  // default included attributes and population spec
  const attributes = ["id", "identifier", "updatedAt", "createdAt"];
  const populationSpec = [
    {
      association: db.Organization.DataSource,
      attributes: ["method", "version", "createdAt"],
      include: { model: db.User, attributes: ["id", "identifier"] },
    },
    {
      association: db.Organization.OrganizationStatuses,
      attributes: ["status", "createdAt"],
      include: { model: db.User, attributes: ["id", "identifier"] },
    },
  ];

  if (needFullInfo) {
    populationSpec.push({
      association: db.Organization.AdditionalChecks,
      attributes: ["name", "data"],
    });
  }

  const orgs = await db.Organization.findAll({
    attributes: needFullInfo ? undefined : attributes,
    include: populationSpec,
    order: [
      ["createdAt", "asc"],
      [db.Organization.OrganizationStatuses, "createdAt", "desc"],
    ],
  });
  res.json(orgs.map((org) => mapToResponseFormat(org.toJSON(), data)));
};

const createOrganizations = async function (req, res) {
  try {
    const { data, process } = req.query;
    const needToProcess = process && process === "true";

    const orgData = { ...req.body };
    // other option is to change aliases for associations
    orgData.AdditionalChecks = orgData.additionalChecks;
    delete orgData.additionalChecks;
    orgData.OrganizationStatuses = [
      {
        status: "CREATED",
        UserId: req.currentUser.id,
      },
    ];

    const source = await db.DataSource.findOne({
      where: { UserId: req.currentUser.id, method: "api", version: "1.0" },
    });
    orgData.DataSourceId = source.id;

    let org;
    const transactionRes = await db.sequelize.transaction(async (t) => {
      org = await db.Organization.create(orgData, {
        transaction: t,
        include: [
          {
            association: db.Organization.AdditionalChecks,
          },
          {
            association: db.Organization.OrganizationStatuses,
          },
        ],
      });
    });

    // we wil use this object to compose response
    org = org.toJSON();

    // it seemed more logical to add "PROCESSING" status only when process query param is true
    if (needToProcess) {
      org.OrganizationStatuses.unshift(
        (
          await db.OrganizationStatus.create({
            status: "PROCESSING",
            UserId: req.currentUser.id,
            OrganizationId: org.id,
          })
        ).toJSON()
      );
      await addProcessOrganizationJob(org, req.currentUser.id);
    }

    // no need to make new calls to DB to populate associations as we already all necessery info

    org.DataSource = source.toJSON();
    org.DataSource.User = req.currentUser;
    org.OrganizationStatuses.forEach(
      (status) => (status.User = req.currentUser)
    );

    res.status(201).json(mapToResponseFormat(org, data));
  } catch (e) {
    res.status(400).json(formatSequelizeError(e));
  }
};

router.get("/", listOrganizations);
router.post("/", createOrganizations);

module.exports = router;
