const express = require("express");
const bodyParser = require("body-parser");
const helmet = require("helmet");

const app = express();

const db = require("./models");
const organizations = require("./routes/organizations");
const { authMiddleware } = require("./routes/auth");

const port = process.env.PORT || 8080;

app.use(helmet());
app.use(bodyParser.json());
app.use(authMiddleware);

app.use("/organizations", organizations);

(async () => {
  await db.waitForDb();
  // while we are in development and might change tables often
  if (process.env.NODE_ENV === "development") {
    // await db.User.sync({ force: true });
    // await db.DataSource.sync({ force: true });
    // await db.Organization.sync({ force: true });
    // await db.AdditionalCheck.sync({ force: true });
    // await db.OrganizationStatus.sync({ force: true });
    await db.sequelize.sync({ force: true });
  }

  app.listen(port, function () {
    console.log(`Example app listening on ${port}!`);
  });
})();
