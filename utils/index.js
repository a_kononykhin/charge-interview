const { ValidationError } = require("sequelize");

exports.formatSequelizeError = function (err) {
  const formatted = {};
  // this is validation error, extract messages and paths
  if (err instanceof ValidationError) {
    formatted.errors = err.errors.map((fieldError) => ({
      message: fieldError.message,
      path: fieldError.path,
    }));
  } else {
    formatted.message = err.message;
  }

  return formatted;
};
