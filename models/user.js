"use strict";

// TODO: add validations/constraints
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      api_token: { type: DataTypes.STRING, unique: true },
      identifier: { type: DataTypes.STRING, allowNull: false, unique: true },
    },
    {}
  );
  User.associate = function ({ User, DataSource }) {
    User.hasMany(DataSource, {
      foreignKey: {
        allowNull: false,
      },
      onDelete: "cascade",
    });
  };
  return User;
};
