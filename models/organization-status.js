"use strict";

// TODO: add validations/constraints
module.exports = (sequelize, DataTypes) => {
  const OrganizationStatus = sequelize.define(
    "OrganizationStatus",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: { notNull: true },
      },
    },
    {
      updatedAt: false,
    }
  );
  OrganizationStatus.associate = function ({ User }) {
    OrganizationStatus.belongsTo(User, {
      foreignKey: {
        allowNull: false,
      },
    });
  };
  return OrganizationStatus;
};
