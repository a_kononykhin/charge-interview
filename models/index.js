"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || "development";
const config = require("../config/config.js")[env];
const db = {};

let sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
);

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach((file) => {
    const model = sequelize["import"](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

let triesLeft = 5;
db.waitForDb = async () => {
  while (true) {
    try {
      // to check if we connected
      await db.sequelize.authenticate();
      break;
    } catch (e) {
      if (triesLeft == 0) {
        throw new Error("Couldn't connect to database");
      }
      console.log(`Couldn't connect to database, tries left: ${triesLeft}`);
      triesLeft--;
      await new Promise((resolve) => setTimeout(() => resolve(), 2000));
    }
  }
};

module.exports = db;
