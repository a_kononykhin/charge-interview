"use strict";

// TODO: add validations/constraints
module.exports = (sequelize, DataTypes) => {
  const DataSource = sequelize.define(
    "DataSource",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      method: { type: DataTypes.STRING, allowNull: false },
      version: { type: DataTypes.STRING },
    },
    {
      updatedAt: false,
    }
  );
  DataSource.associate = function ({ User, DataSource, Organization }) {
    DataSource.belongsTo(User, {
      foreignKey: {
        allowNull: false,
      },
      onDelete: "cascade",
    });
  };
  return DataSource;
};
