"use strict";
module.exports = (sequelize, DataTypes) => {
  const AdditionalCheck = sequelize.define(
    "AdditionalCheck",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      data: {
        type: DataTypes.JSON,
        allowNull: false,
        validate: {
          notNull: true,
        },
      },
    },
    {
      timestamps: false,
    }
  );
  AdditionalCheck.associate = function ({}) {};
  return AdditionalCheck;
};
