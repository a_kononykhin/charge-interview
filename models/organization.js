"use strict";

// TODO: add string length validations
// TODO: add enums for some fileds (businessType, bankAccountType)

module.exports = (sequelize, DataTypes) => {
  const Organization = sequelize.define(
    "Organization",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      accountId: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      externalId: {
        type: DataTypes.UUID,
        defaultValue: null,
      },
      identifier: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: {
          msg: "This identifier is already taken",
        },
        validate: {
          isEmail: true,
          notNull: true,
        },
      },
      businessName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      dba: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      ein: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
          isNumeric: true,
        },
      },
      businessType: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      bankAccountType: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      businessStreetAddress: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      businessCity: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      businessState: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      businessZip: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
          isNumeric: true,
        },
      },
      businessCountry: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      contactFirstName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      contactLastName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      contactTitle: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      contactPhone: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
          isNumeric: true,
        },
      },
      contactEmail: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
          isEmail: true,
        },
      },
      website: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
          isUrl: true,
        },
      },
      ip: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
          isIP: true,
        },
      },
      deviceFingerprint: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          notNull: true,
        },
      },
      meta: { type: DataTypes.JSON, defaultValue: {} },
    },
    {}
  );
  Organization.associate = function ({
    AdditionalCheck,
    DataSource,
    OrganizationStatus,
  }) {
    Organization.AdditionalChecks = Organization.hasMany(AdditionalCheck, {
      foreignKey: {
        allowNull: false,
      },
      onDelete: "cascade",
    });

    Organization.DataSource = Organization.belongsTo(DataSource, {
      foreignKey: {
        allowNull: false,
      },
    });

    Organization.OrganizationStatuses = Organization.hasMany(
      OrganizationStatus,
      {
        foreignKey: {
          allowNull: false,
        },
      }
    );
  };
  return Organization;
};
